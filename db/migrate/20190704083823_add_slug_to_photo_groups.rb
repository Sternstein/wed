class AddSlugToPhotoGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :photo_groups, :slug, :string
    add_index :photo_groups, :slug, unique: true
  end
end
