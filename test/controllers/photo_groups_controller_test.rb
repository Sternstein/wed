require 'test_helper'

class PhotoGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @photo_group = photo_groups(:one)
  end

  test "should get index" do
    get photo_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_photo_group_url
    assert_response :success
  end

  test "should create photo_group" do
    assert_difference('PhotoGroup.count') do
      post photo_groups_url, params: { photo_group: { title: @photo_group.title } }
    end

    assert_redirected_to photo_group_url(PhotoGroup.last)
  end

  test "should show photo_group" do
    get photo_group_url(@photo_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_photo_group_url(@photo_group)
    assert_response :success
  end

  test "should update photo_group" do
    patch photo_group_url(@photo_group), params: { photo_group: { title: @photo_group.title } }
    assert_redirected_to photo_group_url(@photo_group)
  end

  test "should destroy photo_group" do
    assert_difference('PhotoGroup.count', -1) do
      delete photo_group_url(@photo_group)
    end

    assert_redirected_to photo_groups_url
  end
end
