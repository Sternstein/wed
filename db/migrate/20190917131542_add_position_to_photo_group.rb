class AddPositionToPhotoGroup < ActiveRecord::Migration[5.2]
  def change
    add_column :photo_groups, :position, :integer
  end
end
