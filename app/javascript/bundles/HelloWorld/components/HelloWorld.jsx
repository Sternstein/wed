import PropTypes from 'prop-types';
import React from 'react';
import Gallery from 'react-grid-gallery';

export default class HelloWorld extends React.Component {

 constructor(props) {
 	super(props);
 }

  render() {
    return (
      <Gallery images={this.props.images} enableImageSelection={false} imageCountSeparator={" из "} />
    );
  }

}
