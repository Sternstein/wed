class PhotoGroupSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes :images
  def images
	photo_array = Photo.where(photo_group_id: self.object.id).order(:position)  
    	photo_array.map do |photo|
        {caption: photo.caption,
         thumbnailWidth: photo.width,
         thumbnailHeigh: photo.height,
         thumbnail: rails_representation_url(photo.image.variant(resize: "300x300")),
         src: rails_representation_url(photo.image.variant(resize: "1000x1000"))}
    end
  end

end
