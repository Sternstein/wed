json.extract! photo, :id, :title, :photo_group_id, :image
json.url photo_url(photo, format: :json)
