class GalleryController < ApplicationController
  before_action :set_photo_group, only: [:show]
  layout "hello_world"

  def index
    @photo_groups = PhotoGroup.all.order(:position)
  end

  def show
      @photo_groups = PhotoGroup.all.order(:position)
      @photos = PhotoGroupSerializer.new(@photo_group).to_json
  end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo_group
      @photo_group = PhotoGroup.friendly.find(params[:id])
    end

end
