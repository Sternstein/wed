require "application_system_test_case"

class PhotoGroupsTest < ApplicationSystemTestCase
  setup do
    @photo_group = photo_groups(:one)
  end

  test "visiting the index" do
    visit photo_groups_url
    assert_selector "h1", text: "Photo Groups"
  end

  test "creating a Photo group" do
    visit photo_groups_url
    click_on "New Photo Group"

    fill_in "Title", with: @photo_group.title
    click_on "Create Photo group"

    assert_text "Photo group was successfully created"
    click_on "Back"
  end

  test "updating a Photo group" do
    visit photo_groups_url
    click_on "Edit", match: :first

    fill_in "Title", with: @photo_group.title
    click_on "Update Photo group"

    assert_text "Photo group was successfully updated"
    click_on "Back"
  end

  test "destroying a Photo group" do
    visit photo_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Photo group was successfully destroyed"
  end
end
