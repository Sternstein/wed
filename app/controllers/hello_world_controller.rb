# frozen_string_literal: true

class HelloWorldController < ApplicationController
  layout "hello_world"

  def index
      @photki = PhotoGroup.first.photos.map { |photo| PhotoSerializer.new(photo).to_h }
      h1 = Hash.new
      h1['images'] = @photki
      @photos = h1.to_json
    @hello_world_props = @photos
  end
end
