Rails.application.routes.draw do
  get 'gallery/' => 'gallery#index'
  get 'gallery/:id' => 'gallery#show', as: :gallery_show
  get 'hello_world', to: 'hello_world#index'
  devise_for :users

  resources :photo_groups do
    collection do
      patch :sort
    end
  end

  resources :photos  do
    collection do
      patch :sort
    end
  end

  delete "photo_group/images/:id", to: 'photo_groups#delete_image_attachment', as: :delete_photo
  get "/pages/:page" => "pages#show"
  get "about" => "pages#show", page: "about"
  get 'photo_group/:id/photos', to: 'photo_groups#api_photos', as: :api_photos
  root "gallery#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
