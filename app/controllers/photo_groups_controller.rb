class PhotoGroupsController < ApplicationController
  before_action :set_photo_group, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index, :new, :create, :show, :edit, :update, :destroy]

  def api_photos
      @photo_group = PhotoGroup.find(params[:id])
      @group_photos = @photo_group.photos
      render json: @group_photos
  end

  def sort
    params[:photo_group].each_with_index do |id, index|
      PhotoGroup.where(id: id).update_all(position: index + 1)
    end
  end

  def delete_image_attachment
    @image = ActiveStorage::Attachment.find_by(id: params[:id])
    puts "try delete #{@image}"
    @image.purge_later
    redirect_back(fallback_location: request.referer)
  end

  # GET /photo_groups
  # GET /photo_groups.json
  def index
    @photo_groups = PhotoGroup.all
  end

  # GET /photo_groups/1
  # GET /photo_groups/1.json
  def show
      @photki = @photo_group.photos.map { |photo| PhotoSerializer.new(photo).to_h }
      h = Hash.new
      h['images'] = @photki
      @photos = h.to_json
  end

  # GET /photo_groups/new
  def new
    @photo_group = PhotoGroup.new
  end

  # GET /photo_groups/1/edit
  def edit
  end

  # POST /photo_groups
  # POST /photo_groups.json
  def create
    @photo_group = PhotoGroup.new(photo_group_params)

    respond_to do |format|
      if @photo_group.save
        @photo_group.images.each do |img|
          photo = Photo.new(title: @photo_group.title, width: 300, height: 300, caption: '', photo_group_id: @photo_group.id)
          if photo.save
          photo.image.attach(img.blob)
          end
        end
        format.html { redirect_to @photo_group, notice: 'Фотогруппа успешно создана.' }
        format.json { render :show, status: :created, location: @photo_group }
      else
        format.html { render :new }
        format.json { render json: @photo_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /photo_groups/1
  # PATCH/PUT /photo_groups/1.json
  def update
    respond_to do |format|
      if @photo_group.update(photo_group_params)
        format.html { redirect_to @photo_group, notice: 'Фотогруппа успешно обновлена.' }
        format.json { render :show, status: :ok, location: @photo_group }
      else
        format.html { render :edit }
        format.json { render json: @photo_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photo_groups/1
  # DELETE /photo_groups/1.json
  def destroy
    @photo_group.destroy
    respond_to do |format|
      format.html { redirect_to photo_groups_url, notice: 'Photo group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo_group
      @photo_group = PhotoGroup.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photo_group_params
      params.require(:photo_group).permit(:title, :group_image, :group_image_sm, images: [])
    end
end
