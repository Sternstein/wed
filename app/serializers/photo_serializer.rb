class PhotoSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :thumbnailHeight, :thumbnailWidth, :caption, :src, :thumbnail
  def src
      rails_blob_url(object.image) if object.image.attached?
  end
  def thumbnail
      rails_blob_url(object.image) if object.image.attached?
  end
  def thumbnailWidth
    object.width
  end
  def thumbnailHeight
    object.height
  end
end
