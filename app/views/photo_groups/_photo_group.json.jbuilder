json.extract! photo_group, :id, :title
json.url api_photos_url(photo_group, format: :json)
