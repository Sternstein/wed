class AddPhotoGroupRefToPhotos < ActiveRecord::Migration[5.2]
  def change
    add_reference :photos, :photo_group, foreign_key: true
  end
end
