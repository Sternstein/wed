class PhotoGroup < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged
  has_one_attached :group_image
  has_one_attached :group_image_sm
  has_many_attached :images
  has_many :photos, dependent: :destroy

  validates :title, presence: true
  validates :group_image, attached: true, size: { less_than: 10.megabytes , message: 'Слишком большой размер' }, content_type: ['image/png', 'image/jpg', 'image/jpeg']
  validates :group_image_sm, attached: true, size: { less_than: 10.megabytes , message: 'Слишком большой размер' }, content_type: ['image/png', 'image/jpg', 'image/jpeg']
  # validates :images, attached: true, size: { less_than: 100.megabytes , message: 'is not given between size' }, content_type: ['image/png', 'image/jpg', 'image/jpeg']
end
